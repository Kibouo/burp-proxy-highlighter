# Burp Proxy Highlighter
Sets color in HTTP history tab based on incoming proxy port. Useful to keep sessions separate.