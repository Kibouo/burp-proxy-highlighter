# Burp need to load each class explicitly
from burp import IBurpExtender, IProxyListener

# This allow us to get typing hints for all burp classes in our IDE
from burp import *

HIGHLIGHTER_COLORS = ["red", "orange", "yellow", "green", "cyan", "blue", "pink", "magenta", "gray", None]
OFFSET = 0

class BurpExtender(IBurpExtender):
    _callbacks = None
    _helpers = None

    def registerExtenderCallbacks(self, callbacks):
        self._callbacks = callbacks
        self._helpers = callbacks.getHelpers()

        self._callbacks.setExtensionName("Proxy Highlighter")
        self._callbacks.registerProxyListener(ProxyListener())

        print("Plugin loaded")


class ProxyListener(IProxyListener):
    def processProxyMessage(self, is_request, message):
        try:
            if is_request:
                color_index = int(message.getListenerInterface().split(':')[-1][-1])
                message.getMessageInfo().setHighlight(HIGHLIGHTER_COLORS[color_index])
        except Exception as e:
            print(e)